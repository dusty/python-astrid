.. Astrid Python API documentation master file, created by
   sphinx-quickstart on Wed Oct 10 19:40:09 2012.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Astrid Python API's documentation!
=============================================

Contents:

.. automodule:: astrid
   :members:

